//
//  GameScene.swift
//  MadMermaid
//
//  Created by Haci Kale on 24/03/15.
//  Copyright (c) 2015 Haci Kale. All rights reserved.
//


import SpriteKit

class GameScene: SKScene, SKPhysicsContactDelegate
{
    var mermaid = SKSpriteNode()
    var food = SKSpriteNode()
    let mermaidGroup:UInt32=0
    let objectGroup:UInt32=1
    let foodGroup:UInt32=2
    let food2Group:UInt32=8
    let sharkGroup:UInt32=4
    var scoreLabel = SKLabelNode(fontNamed: "Jack's Candlestick")
    var score=0
    var movingObjects = SKNode()
    var gameOverLabel = SKLabelNode(fontNamed: "Jack's Candlestick")
    var lastUpdateTime: NSTimeInterval = 0
    var dt: NSTimeInterval = 0
    let mermaidMovePointsPerSec: CGFloat = 1280.0
    var velocity = CGPointZero
    var lastTouchLocation: CGPoint?

    
    override func didMoveToView(view: SKView)
    {
        

        /* Setup your scene here */
        physicsWorld.gravity = CGVectorMake(0,0)
        physicsWorld.contactDelegate = self
        
        addChild(movingObjects)
        createBackground()
        createMermaid()
        Ground()
        loft()
        
        //create Score label
        scoreLabel.fontSize=100
        scoreLabel.position=CGPoint(x: CGRectGetMidX(self.frame), y: CGRectGetMidY(self.frame)+450)
        scoreLabel.text="Score:\(score)"
        scoreLabel.zPosition=140
        addChild(scoreLabel)

        //create game over label
        gameOverLabel.fontSize=300
        gameOverLabel.position=CGPoint(x: CGRectGetMidX(self.frame), y: CGRectGetMidY(self.frame)+100)
        gameOverLabel.zPosition=140
        

        runAction(SKAction.repeatActionForever(
            SKAction.sequence([SKAction.runBlock(createShark),
                SKAction.waitForDuration(3.5)])))
       
        runAction(SKAction.repeatActionForever(
                    SKAction.sequence([SKAction.runBlock(createFood),
                        SKAction.waitForDuration(2)])))
        
        runAction(SKAction.repeatActionForever(
            SKAction.sequence([SKAction.runBlock(createFood2),
                SKAction.waitForDuration(25.5)])))
        
    }
    
    
    func didBeginContact(contact: SKPhysicsContact)
    {
        //this gets called automatically when two objects begin contact with each other
        
        let contactMask = contact.bodyA.categoryBitMask | contact.bodyB.categoryBitMask
        
        switch(contactMask) {
            
        case sharkGroup | mermaidGroup:
           //is Shark and Mermaids in contact?
            movingObjects.removeFromParent()
            mermaid.removeFromParent()
            gameOverLabel.text="Game Over"
            addChild(gameOverLabel)
            
            
        case foodGroup | mermaidGroup:
            //is food and Mermaid in contact?
            score++
            scoreLabel.text="Score: \(score)"
            destroyFood(contact.bodyB.node as SKSpriteNode)
            
        case food2Group | mermaidGroup:
            
            //is food2 and mermaid in contact?
            score++
            score++
            scoreLabel.text="Score: \(score)"
            destroyFood(contact.bodyB.node as SKSpriteNode)
            
        default:
            return
            
        }
        
    }
    
    
    func destroyFood(sprite: SKSpriteNode)
    {
          sprite.removeFromParent()
    }
    
    func Ground()
    {
        var ground = SKNode()
        ground.position=CGPoint(x: 0, y: 150)
        ground.physicsBody = SKPhysicsBody(rectangleOfSize: CGSize(width: size.width*4, height: 200))
        ground.physicsBody?.dynamic = false
        ground.physicsBody?.categoryBitMask=objectGroup
        
        self.addChild(ground)
    }
    
    func loft()
    {
        var loft = SKNode()
        loft.position=CGPoint(x: 0, y: 1400)
        loft.physicsBody = SKPhysicsBody(rectangleOfSize: CGSize(width: size.width*4, height: 200))
        loft.physicsBody?.dynamic = false
        loft.physicsBody?.categoryBitMask=objectGroup
        
        self.addChild(loft)
    }

    
    func createBackground()
    {
        var backgroundTexture = SKTexture(imageNamed: "waterbg.png")
        var background = SKSpriteNode(texture: backgroundTexture)
        background.position = CGPoint(x: 0 + backgroundTexture.size().width , y:CGRectGetMidY(self.frame))
        background.speed = 1
        background.setScale(2.2)
        background.zPosition = -1 /*gør at baggrunden er bag alt*/
        addChild(background)
        
    }
    
    func moveSprite(sprite: SKSpriteNode, velocity: CGPoint)
    {
        let amountToMove = velocity * CGFloat(dt)
        sprite.position += amountToMove
    }
    
    func moveMermaidToward(location: CGPoint)
    {
        //startMermaidAnimation()
        let offset = location - mermaid.position
        let direction = offset.normalized()
        velocity = direction * mermaidMovePointsPerSec
    }
    
    func sceneTouched(touchLocation:CGPoint)
    {
        lastTouchLocation = touchLocation
        moveMermaidToward(touchLocation)
       
    }
    
    override func touchesBegan(touches: NSSet, withEvent event: UIEvent)
    {
            let touch = touches.anyObject() as UITouch
            let touchLocation = touch.locationInNode(self)
            sceneTouched(touchLocation)
    }
    
    override func touchesMoved(touches: NSSet, withEvent event: UIEvent)
    {
            let touch = touches.anyObject() as UITouch
            let touchLocation = touch.locationInNode(self)
            sceneTouched(touchLocation)
    }
    
    func createShark()
    {
        var shark = SKSpriteNode(imageNamed: "shark1.png")
        shark.name = "shark"
        shark.position = CGPoint(x:size.width + shark.size.width/2, y: CGFloat.random(min: 300, max: 1200))
        var actionMove = SKAction.moveToX(-shark.size.width/2, duration: 2.0)
        var actionRemove = SKAction.removeFromParent()
        shark.runAction(SKAction.sequence([actionMove, actionRemove]))
        

        /*this is the shark*/
        var sharkTex1 = SKTexture(imageNamed: "shark1.png")
        var sharkTex2 = SKTexture(imageNamed: "shark2.png");
        
        /*this is what makes the shark switch between 2 looks so that the wings move*/
        var alternateTexture = SKAction.animateWithTextures([sharkTex1, sharkTex2], timePerFrame: 0.1)
        var repeatForever = SKAction.repeatActionForever(alternateTexture)
        /*Run the repeatForever action on the shark*/
        shark.runAction(repeatForever)
        
        
        shark.setScale(3.5)
        /*Give the shark a positive zPosition to ensure it's in the foreground*/
        shark.zPosition = 100
        shark.physicsBody = SKPhysicsBody(circleOfRadius: shark.size.height/2)
        shark.physicsBody?.categoryBitMask = sharkGroup
        shark.physicsBody?.collisionBitMask = objectGroup
        shark.physicsBody?.contactTestBitMask = objectGroup
        
        shark.physicsBody?.allowsRotation = false
        shark.physicsBody?.angularVelocity = 0
        
        /*Add the shark as a child to the GameScene*/
        movingObjects.addChild(shark)
    }

    func createFood()
    {
        // 1
        var star = SKSpriteNode(imageNamed: "star")
        star.name = "star"
        star.position = CGPoint(x:CGFloat.random(min: 40, max: 1900), y: CGFloat.random(min: 300, max: 1200))
        movingObjects.addChild(star)
        // 2
        var appear = SKAction.scaleTo(0.3, duration: 0.5)
        
        star.zRotation = -π / 16.0
        var leftWiggle = SKAction.rotateByAngle(π/8.0, duration: 0.5)
        var rightWiggle = leftWiggle.reversedAction()
        var fullWiggle = SKAction.sequence([leftWiggle, rightWiggle])
        var scaleUp = SKAction.scaleBy(1.2, duration: 0.25)
        var scaleDown = scaleUp.reversedAction()
        var fullScale = SKAction.sequence(
            [scaleUp, scaleDown, scaleUp, scaleDown])
        var group = SKAction.group([fullScale, fullWiggle])
        var groupWait = SKAction.repeatAction(group, count: 10)
        
        var disappear = SKAction.scaleTo(0, duration: 0.5)
        var removeFromParent = SKAction.removeFromParent()
        var actions = [appear, groupWait, disappear, removeFromParent]
        star.runAction(SKAction.sequence(actions))
        
        /*Give the star a positive zPosition to ensure it's in the foreground*/
        star.zPosition = 100
        star.physicsBody = SKPhysicsBody(circleOfRadius: star.size.height/2)
        star.physicsBody?.categoryBitMask = foodGroup
        star.physicsBody?.collisionBitMask = mermaidGroup
        star.physicsBody?.contactTestBitMask = objectGroup
        
        star.physicsBody?.allowsRotation = false
        star.physicsBody?.angularVelocity = 0
    }
    
    
    func createFood2()
    {
        // 1
        var octo = SKSpriteNode(imageNamed: "octo")
        octo.name = "octo"
        octo.position = CGPoint(x: CGFloat.random(min: 50, max: 1900), y:33)
        movingObjects.addChild(octo)
        
        // 2
        var appear = SKAction.scaleTo(0.5, duration: 0.5)
        var actionMove = SKAction.moveToY(octo.size.height*4, duration: 6.0)
        var disappear = SKAction.scaleTo(0, duration: 0.5)
        var removeFromParent = SKAction.removeFromParent()
        var actions = [appear,actionMove,disappear, removeFromParent]
        octo.runAction(SKAction.sequence(actions))
        
        /*Give the octo a positive zPosition to ensure it's in the foreground*/
        octo.zPosition = 100
        /*This is the phsysicsbody used for collision*/
        octo.physicsBody = SKPhysicsBody(circleOfRadius: octo.size.height/2)
        octo.physicsBody?.categoryBitMask = food2Group
        octo.physicsBody?.collisionBitMask = mermaidGroup
        octo.physicsBody?.contactTestBitMask = mermaidGroup
        octo.physicsBody?.allowsRotation = false
        octo.physicsBody?.angularVelocity = 0
        
        
    }
    
    func createMermaid()
    {
        
        /*this is the mermaid*/
        var mermaidTex1 = SKTexture(imageNamed: "mermaid1.png")
        var mermaidTex2 = SKTexture(imageNamed: "mermaid2.png");
        var mermaidTex3 = SKTexture(imageNamed: "mermaid3.png");
        mermaid = SKSpriteNode(texture: mermaidTex1)
        
        /*this is what makes the mermaid switch between 2 looks so that the wings move*/
        var alternateTexture = SKAction.animateWithTextures([mermaidTex1, mermaidTex2, mermaidTex3], timePerFrame: 0.1)
        var repeatForever = SKAction.repeatActionForever(alternateTexture)
        
        /*Run the repeatForever action on the mermaid*/
        mermaid.runAction(repeatForever)
        mermaid.position = CGPoint(x: 400, y: 400)
        mermaid.setScale(1.8)
        
        /*Give the mermaid a positive zPosition to ensure it's in the foreground*/
        mermaid.zPosition = 100
        mermaid.physicsBody = SKPhysicsBody(circleOfRadius: mermaid.size.height/2)
        mermaid.physicsBody?.categoryBitMask = mermaidGroup
        mermaid.physicsBody?.collisionBitMask = objectGroup
        mermaid.physicsBody?.contactTestBitMask = sharkGroup | foodGroup | food2Group
        
        mermaid.physicsBody?.allowsRotation = false
        mermaid.physicsBody?.angularVelocity = 0
        
        /*Add the mermaid as a child to the GameScene*/
        addChild(mermaid)
    }
    
    
    override func update(currentTime: CFTimeInterval)
    {
        
        if lastUpdateTime > 0 {
            dt = currentTime - lastUpdateTime
        } else {
            dt = 0
        }
        lastUpdateTime = currentTime
        
        if let lastTouch = lastTouchLocation
        {
            let diff = lastTouch - mermaid.position
            if (diff.length() <= mermaidMovePointsPerSec * CGFloat(dt))
            {
                mermaid.position = lastTouchLocation!
                velocity = CGPointZero
            } else
            {
                moveSprite(mermaid, velocity: velocity)
              
               
            }
        }
    }
}

